package com.example.androidacademyhw1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener((v) -> {
            Intent intent = new Intent(this, EmailActivity.class);
            intent.putExtra("text", ((TextView)findViewById((R.id.editText))).getText().toString());
            startActivity(intent);
        });
    }
}
