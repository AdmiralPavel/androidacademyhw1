package com.example.androidacademyhw1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class EmailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);


        Intent intent = getIntent();
        TextView textView = findViewById(R.id.textView);
        String text = intent.getStringExtra("text");
        textView.setText(text);


        Button button = findViewById(R.id.email_button);
        button.setOnClickListener((v) -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, "admiralpavel99@gmail.com");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Android Academy");
            emailIntent.setData(Uri.parse("mailto:" + "admiralpavel99@gmail.com"));
            emailIntent.putExtra(Intent.EXTRA_TEXT, text);
            if (emailIntent.resolveActivity(getPackageManager()) != null)
                startActivity(emailIntent);
            else{
                Toast.makeText(this, R.string.toast_string, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
